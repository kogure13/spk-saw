<?php

class Ranking extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Ranking_model');
    }

    public function index()
    {
        $data['judul'] = "PT. Kahatex | Ranking";
        $data['user'] = $this->session->userdata('role');
        $data['hitungAwal'] = $this->Ranking_model->hitungAwal();

        $this->load->view('templates/header', $data);
        $this->load->view('ranking/index', $data);
        $this->load->view('templates/footer');
    }
}