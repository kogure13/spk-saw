<?php

class Home extends CI_Controller {
    
    public function index() {
        $data['judul'] = 'PT. Kahatex - SPK Metode SAW';
        $data['user'] = $this->db->get_where('karyawan', ['username' => $this->session->userdata['username']])->row_array();
                
        $this->load->view('home/index', $data);        
    }
}