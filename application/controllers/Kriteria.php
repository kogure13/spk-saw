<?php

class Kriteria extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        is_logged_in();

        $this->load->model('Kriteria_model');
    }

    public function index()
    {
        $data['judul'] = "PT. Kahatex | Kriteria SPK";
        $data['kriteria'] = $this->Kriteria_model->getAllKriteria();
        $data['user'] = $this->session->userdata('role');

        $this->load->view('templates/header', $data);
        $this->load->view('kriteria/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
        $data['judul'] = "PT. Kahatex | Form Tambah Data Kriteria";

        $this->form_validation->set_rules('kode', 'Kode Kriteria', 'required');
        $this->form_validation->set_rules('kriteria', 'Kriteria', 'required');
        $this->form_validation->set_rules('sifat', 'Sifat Kriteria', 'required');
        $this->form_validation->set_rules('bobot', 'Bobot Kriteria', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('kriteria/tambah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Kriteria_model->tambahKriteria();
            $this->session->set_flashdata('flash', 'Ditambahkan');
            redirect('Kriteria');
        }
    }

    public function tambahSubKriteria()
    {
        $data['judul'] = "PT. Kahatex | Form Tambah Data Sub Kriteria";
        $data['kriteria'] = $this->Kriteria_model->getAllKriteria();

        $this->form_validation->set_rules('sub_kriteria', 'Sub Kriteria', 'required');
        $this->form_validation->set_rules('nilai', 'nilai Kriteria', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('kriteria/tambahSubKriteria', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Kriteria_model->tambahSubKriteria();
            $this->session->set_flashdata('flash', 'Ditambahkan');
            redirect('Kriteria');
        }
    }

    public function edit($id)
    {
        $data['judul'] = "PT. Kahatex | Form Rubah Data Kriteria";
        $data['kriteria'] = $this->Kriteria_model->getKriteriaByID($id);

        $this->form_validation->set_rules('kriteria', 'Kriteria', 'required');
        $this->form_validation->set_rules('bobot', 'Bobot', 'required|is_numeric');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('kriteria/edit', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Kriteria_model->updateKriteria();
            $this->session->set_flashdata('flash', 'Dirubah');
            redirect('Kriteria');
        }
    }

    public function editSubKriteria($id)
    {
        $data['judul'] = "PT. Kahatex | Form Rubah Data Sub Kriteria";
        $data['kriteria'] = $this->Kriteria_model->getAllKriteria();
        $data['skriteria'] = $this->Kriteria_model->getSubKriteriaByID($id);

        $this->form_validation->set_rules('sub_kriteria', 'Kriteria', 'required');
        $this->form_validation->set_rules('nilai', 'Bobot', 'required|is_numeric');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('kriteria/editSubKriteria', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Kriteria_model->updateSubKriteria();
            $this->session->set_flashdata('flash', 'Dirubah');
            redirect('Kriteria');
        }
    }

    public function detail($id)
    {
        $data['judul'] = "PT. Kahatex | Detail Data Kriteria";
        $data['kriteria'] = $this->Kriteria_model->getKriteriaByID($id);
        $data['skriteria'] = $this->Kriteria_model->getSubKriteriaByIDKriteria($id);

        $this->load->view('templates/header', $data);
        $this->load->view('kriteria/detail', $data);
        $this->load->view('templates/footer');
    }

    public function delete($id)
    {
        $this->Kriteria_model->delete($id);
        $this->session->set_flashdata('flash', 'Dihapus');
        redirect('Kriteria');
    }
}