<?php

class Karyawan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        is_logged_in();

        $this->load->model('Karyawan_model');
        $this->load->model('Kriteria_model');

    }

    // Page
    public function index()
    {
        $data['judul'] = "PT. Kahatex | Data Karyawan";
        $data['karyawan'] = $this->Karyawan_model->getAllKaryawan();
        $data['user'] = $this->session->userdata('role');

        $this->load->view('templates/header', $data);
        $this->load->view('karyawan/index', $data);
        $this->load->view('templates/footer');
    }

    public function profile()
    {
        $data['judul'] = "PT. Kahatex | Profile Karyawan";
        $data['user'] = $this->session->userdata('role');
        $iduser = $this->session->userdata('id');
        // var_dump($iduser); die();
        $data['karyawan'] = $this->Karyawan_model->getKaryawanByID($iduser);

        $this->load->view('templates/header', $data);
        $this->load->view('karyawan/profile', $data);
        $this->load->view('templates/footer');
    }

    // CRUD
    public function tambah()
    {
        $data['judul'] = "PT. Kahatex | Form Tambah Data Karyawan";
        $data['department'] = ['HRD', 'Gudang', 'Logistik', 'Admin Office'];
        $data['user'] = $this->session->userdata('role');

        $this->form_validation->set_rules('nik', 'NIK', 'required');
        $this->form_validation->set_rules('nama', 'Nama Karyawan', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('nohp', 'No. Handphone', 'required|is_numeric');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('karyawan/tambah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Karyawan_model->tambahKaryawan();
            $this->session->set_flashdata('flash', 'Dirubah');
            redirect('Karyawan');
        }
    }

    public function edit($id)
    {
        $data['judul'] = "PT. Kahatex | Form Edit Data Karyawan";
        $data['karyawan'] = $this->Karyawan_model->getKaryawanByID($id);
        $data['user'] = $this->session->userdata('role');
        $data['department'] = ['HRD', 'Gudang', 'Logistik', 'Admin Office'];

        $this->form_validation->set_rules('nik', 'NIK', 'required');
        $this->form_validation->set_rules('nama', 'Nama Karyawan', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('nohp', 'No. Handphone', 'required|is_numeric');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('karyawan/edit', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Karyawan_model->updateKaryawan();
            $this->session->set_flashdata('flash', 'Dirubah');
            redirect('Karyawan');
        }
    }

    public function cuti($id)
    {
        $data['judul'] = "PT. Kahatex | Form Cuti Karyawan";
        $data['user'] = $this->session->userdata('role');
        $data['karyawan'] = $this->Karyawan_model->getKaryawanByID($id);
        $data['kriteria'] = $this->Kriteria_model->getAllKriteria();
        $data['skriteria'] = $this->Kriteria_model->getAllSubKriteria();

        // $this->form_validation->set_rules('form', '', 'required');

        $numCuti = $this->db->get_where('karyawan_nilai', ['id_karyawan' => $this->session->userdata['id']])->num_rows();

        if ($numCuti > 0) {
            $this->session->set_flashdata('flash', 'Anda sudah mengisi data cuti, silakan hubungi admin untuk info lebih lanjut');
            redirect('Karyawan/profile/');
        } else {
            $this->load->view('templates/header', $data);
            $this->load->view('karyawan/cuti', $data);
            $this->load->view('templates/footer');
        }

    }

    public function addCuti()
    {
        $this->Karyawan_model->addFormCuti();
        $this->session->set_flashdata('flash', 'Disimpan');

        if ($this->session->userdata('role') == 1) {
            redirect('Karyawan');
        } else {
            $id = $this->session->userdata('id');
            redirect('user');
        }

    }

}