<div class="container">
    <div class="card mt-2">
        <div class="card-header">
            Form Tambah Data Kriteria
        </div>
        <div class="card-body">
            <?=validation_errors()?>

            <form action="" method="post">
                <div class="mb-3">
                    <label for="kode" class="form-label">Kode Kriteria</label>
                    <input type="text" class="form-control" id="kode" name="kode" placeholder="">
                </div>
                <div class="mb-3">
                    <label for="kriteria" class="form-label">Kriteria</label>
                    <input type="text" class="form-control" id="kriteria" name="kriteria" placeholder="">
                </div>
                <div class="mb-3">
                    <div class="mb-3">
                        <label for="pertanyaan" class="form-label">Pertanyaan</label>
                        <input type="text" class="form-control" id="pertanyaan" name="pertanyaan" placeholder="">
                    </div>
                </div>
                <div class="mb-3">
                    <label for="sifat" class="form-label">Sifat</label>
                    <br />
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="sifat" id="cost" value="Cost">
                        <label class="form-check-label" for="cost">Cost</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="sifat" id="benefit" value="Benefit">
                        <label class="form-check-label" for="benefit">Benefit</label>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="bobot" class="form-label">Bobot</label>
                    <input type="text" class="form-control" id="bobot" name="bobot" placeholder="">
                </div>
                <div class="mb-3">
                    <a href="<?=base_url()?>Kriteria" class="btn btn-sm btn-danger float-right">Kembali</a>
                    <button type="submit" class="btn btn-sm btn-primary mr-1 float-right">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>