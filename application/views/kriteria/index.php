<div class="container">
    <div class="card mt-2">
        <div class="card-header">
            <h5 class="card-title">Data Olah Kriteria</h5>
            <p class="card-text">SPK App</p>
        </div>
        <div class="card-body">
            <?php if ($this->session->flashdata('flash')): ?>
            <div class="alert alert-success alert-dismissible fade show mb-2" role="alert">
                Data Kriteria berhasil <?=$this->session->flashdata('flash')?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php endif;?>
            <a href="<?=base_url()?>Kriteria/tambah" class="btn btn-sm btn-primary mb-2">Tambah Data</a>
            <a href="<?=base_url()?>Kriteria/tambahSubKriteria" class="btn btn-sm btn-danger mb-2 ml-1">Tambah Sub
                Kriteria</a>
            <div class="wy-table-responsive">
                <table class="table wy-table-bordered" id="dtTable">
                    <thead>
                        <tr class="text-center">
                            <th width="10px">No.</th>
                            <th width="220px">Kriteria</th>
                            <th width="100px">Sifat</th>
                            <th width="100px">Bobot</th>
                            <th width="200px">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($kriteria as $data): ?>
                        <tr>
                            <td class="text-right"><?=$data['id']?></td>
                            <td><?=$data['kriteria']?></td>
                            <td><?=$data['sifat']?></td>
                            <td class="text-center"><?=$data['bobot']?></td>
                            <td>
                                <a href="<?=base_url()?>Kriteria/edit/<?=$data['id']?>" class="btn btn-sm btn-warning">
                                    Edit Kriteria
                                </a>
                                <a href="<?=base_url()?>Kriteria/detail/<?=$data['id']?>"
                                    class="btn btn-sm btn-info ml-1">
                                    Detail
                                </a>
                                <a href="<?=base_url()?>Kriteria/delete/<?=$data['id']?>"
                                    class="btn btn-sm btn-danger ml-1" onclick="confirm('Yakin ?')">
                                    Hapus
                                </a>
                            </td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>