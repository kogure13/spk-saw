<div class="container">
    <div class="card mt-2">
        <div class="card-header">
            <?=$judul?>
        </div>
        <div class="card-body">
            <?=validation_errors()?>

            <form action="" method="post">
                <div class="mb-3">
                    <label for="id_kriteria" class="form-label">Kriteria</label>
                    <select name="id_kriteria" id="id_kriteria" class="form-control">
                        <?php foreach ($kriteria as $k): ?>

                        <option value="<?=$k['id']?>"><?=$k['kriteria']?></option>

                        <?php endforeach;?>
                    </select>
                </div>
                <div class="mb-3">
                    <div class="mb-3">
                        <label for="sub_kriteria" class="form-label">Sub Kriteria</label>
                        <input type="text" class="form-control" id="sub_kriteria" name="sub_kriteria" placeholder="">
                    </div>
                </div>
                <div class="mb-3">
                    <div class="mb-3">
                        <label for="nilai" class="form-label">Nilai</label>
                        <input type="text" class="form-control" id="nilai" name="nilai" placeholder="">
                    </div>
                </div>
                <div class="mb-3">
                    <a href="<?=base_url()?>Kriteria" class="btn btn-sm btn-danger float-right">Kembali</a>
                    <button type="submit" class="btn btn-sm btn-primary mr-1 float-right">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>