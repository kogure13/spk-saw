<div class="container">
    <div class="card mt-2">
        <div class="card-header">
            Detail Kriteria
        </div>
        <div class="card-body">
            <b><?=$kriteria['pertanyaan']?></b>

            <ul class="list-group mb-2">
                <?php foreach ($skriteria as $key => $val): ?>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    <?=$val['sub_kriteria']?>
                    <a href="<?=base_url()?>Kriteria/editSubKriteria/<?=$val['id']?>"
                        class="badge badge-primary">Edit</a>
                </li>

                <?php endforeach;?>
            </ul>
            <a href="<?=base_url()?>Kriteria/tambahSubKriteria"
                class="btn btn-sm btn-danger float-right mb-2 ml-1">Tambah Sub
                Kriteria</a>
            <a href="<?=base_url()?>Kriteria" class="btn btn-sm btn-primary float-right">Kembali</a>
        </div>
    </div>
</div>