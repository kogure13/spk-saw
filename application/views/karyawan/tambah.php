<div class="container">
    <div class="card mt-2">
        <div class="card-header">
            Form Tamabh Data Karyawan
        </div>
        <div class="card-body">
            <?=validation_errors()?>

            <form action="" method="post">
                <div class="mb-3">
                    <label for="nik" class="form-label">NIK karyawan</label>
                    <input type="text" class="form-control" id="nik" name="nik" placeholder="">
                </div>
                <div class="mb-3">
                    <label for="nama" class="form-label">Nama karyawan</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="">
                </div>
                <div class="mb-3">
                    <label for="gender" class="form-label">Gender</label>
                    <br />
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="Laki-laki" value="Laki-laki">
                        <label class="form-check-label" for="laki-laki">Laki-laki</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="Perempuan" value="Perempuan">
                        <label class="form-check-label" for="Perempuan">Perempuan</label>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="department" class="form-label">Department</label>
                    <select name="department" id="department" class="form-control">
                        <?php foreach ($department as $dept): ?>
                        <option value="<?=$dept?>"><?=$dept?></option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">E-mail karyawan</label>
                    <input type="text" class="form-control" id="email" name="email" placeholder="">
                </div>
                <div class="mb-3">
                    <label for="nohp" class="form-label">No. HP karyawan</label>
                    <input type="text" class="form-control" id="nohp" name="nohp" placeholder="">
                </div>
                <div class="mb-3">
                    <a href="<?=base_url()?>karyawan" class="btn btn-sm btn-danger float-right">Kembali</a>
                    <button type="submit" class="btn btn-sm btn-primary mr-1 float-right">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>