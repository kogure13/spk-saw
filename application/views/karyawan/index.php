<div class="container">
    <div class="card mt-2">
        <div class="card-header">
            <h5 class="card-title">Data Karyawan</h5>
            <p class="card-text">PT. Kahatex</p>
        </div>

        <div class="card-body">
            <?php if ($this->session->flashdata('flash')): ?>
            <div class="alert alert-success alert-dismissible fade show mb-2" role="alert">
                Data Karyawan berhasil <?=$this->session->flashdata('flash')?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php endif;?>
            <a href="<?=base_url()?>Karyawan/tambah" class="btn btn-primary mb-2">Tambah Data</a>

            <div class="wy-table-responsive">
                <table class="table" id="dtTable">
                    <thead>
                        <tr>
                            <th width="10px">No.</th>
                            <th width="120px">NIK</th>
                            <th width="420px">Nama Karyawan</th>
                            <th width="200px">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($karyawan as $data): ?>
                        <tr>
                            <td><?=$data['id']?></td>
                            <td><?=$data['nik']?></td>
                            <td><?=$data['nama']?></td>
                            <td>
                                <a href="<?=base_url()?>Karyawan/edit/<?=$data['id']?>" class="btn btn-sm btn-warning">
                                    Edit
                                </a>                                
                            </td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>