<div class="container">
    <div class="card mt-2">
        <div class="card-header">
            Form Edit Data Karyawan
        </div>
        <div class="card-body">
            <?=validation_errors()?>

            <form action="" method="post">
                <input type="hidden" name="id" value="<?=$karyawan['id']?>">
                <div class="mb-3">
                    <label for="nik" class="form-label">NIK karyawan</label>
                    <input type="text" class="form-control" id="nik" name="nik" placeholder=""
                        value="<?=$karyawan['nik']?>">
                </div>
                <div class="mb-3">
                    <label for="nama" class="form-label">Nama karyawan</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder=""
                        value="<?=$karyawan['nama']?>">
                </div>
                <div class="mb-3">
                    <label for="gender" class="form-label">Gender</label>
                    <br />
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="Laki-laki" value="Laki-laki"
                            <?=$karyawan['gender'] == "Laki-laki" ? "checked" : ""?>>
                        <label class="form-check-label" for="laki-laki">Laki-laki</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="Perempuan" value="Perempuan"
                            <?=$karyawan['gender'] == "Perempuan" ? "checked" : ""?>>
                        <label class="form-check-label" for="Perempuan">Perempuan</label>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="department" class="form-label">Department</label>
                    <select name="department" id="department" class="form-control">
                        <?php foreach ($department as $dept): ?>
                        <?php if ($dept == $karyawan['department']): ?>
                        <option value="<?=$dept?>" selected><?=$dept?></option>
                        <?php else: ?>
                        <option value="<?=$dept?>"><?=$dept?></option>
                        <?php endif;?>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">E-mail karyawan</label>
                    <input type="text" class="form-control" id="email" name="email" placeholder=""
                        value="<?=$karyawan['email']?>">
                </div>
                <div class="mb-3">
                    <label for="nohp" class="form-label">No. HP karyawan</label>
                    <input type="text" class="form-control" id="nohp" name="nohp" placeholder=""
                        value="<?=$karyawan['nohp']?>">
                </div>
                <div class="mb-3">
                    <a href="<?=base_url()?>karyawan\profile\<?=$this->session->userdata('id')?>" class="btn btn-sm btn-danger float-right">Kembali</a>
                    <button type="submit" class="btn btn-sm btn-primary mr-1 float-right">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>