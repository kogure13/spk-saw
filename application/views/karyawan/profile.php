<div class="container">
    <div class="card mt-2" style="width: 18rem;">
        <img src="/assets/nophoto.png" class="card-img-top" alt="..."
            style="width: 125px; height: 125px; margin: 20px 80px">
        <div class="card-body">
            <h6 class="card-title"><?=$judul?></h6>
            <p class="card-text">
            <ul>
                <li>NIK : <?=$karyawan['nik'];?></li>
                <li>Nama : <?=$karyawan['nama'];?></li>
            </ul>
            </p>
        </div>

        <div class="card-body">
            <?php if ($this->session->flashdata('flash')): ?>
            <div class="alert alert-danger alert-dismissible fade show mb-2" role="alert">
                <?=$this->session->flashdata('flash')?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php endif;?>
            <a href="<?=base_url()?>Karyawan/edit/<?=$karyawan['id']?>" class="btn btn-sm btn-warning">
                Update
            </a>
            <a href="<?=base_url()?>Karyawan/cuti/<?=$karyawan['id']?>" class="btn btn-sm btn-primary">
                Cuti
            </a>
        </div>
    </div>
</div>