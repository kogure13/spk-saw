<div class="container">
    <div class="card mt-2">
        <div class="card-header">
            <h5 class="card-title">
                <?=$judul?>
            </h5>
            <p class="card-text"></p>
        </div>
        <div class="card-body">
            <ul class="list-group mb-2">
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Nama
                    <span class="badge text-primary"><?=$karyawan['nama']?></span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Bagian/Department
                    <span class="badge text-primary"><?=$karyawan['department']?></span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Tanggal
                    <span class="badge text-primary"><?=date('d-M-Y')?></span>
                </li>
            </ul>

            <?=validation_errors();?>
            <form action="<?=base_url()?>Karyawan/addCuti" method="post" name="form">
                <input type="hidden" name="id" value="<?=$karyawan['id']?>">

                <?php $i = 0;foreach ($kriteria as $k): ?>
                <div class="card mb-2">
                    <div class="card-header text-bold">
                        <?=++$i?>. <?=$k['pertanyaan']?>
                    </div>
                    <div class="card-body">
                        <?php $j = 1;foreach ($skriteria as $sk): ?>
                        <?php if ($sk['id_kriteria'] == $k['id']): ?>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="q<?=$sk['id_kriteria']?>"
                                value="<?=$sk['id']?>">
                            <label class="form-check-label" for="<?=$sk['sub_kriteria']?>">
                                <?=$sk['sub_kriteria']?>
                            </label>
                        </div>
                        <?php endif;?>

                        <?php $j++;endforeach;?>
                    </div>
                </div>
                <?php endforeach;?>

                <button type="submit" class="btn btn-sm btn-success float-right">Simpan</button>
                <a href="<?=base_url()?>Karyawan" class="btn btn-sm btn-primary float-right mr-2">Kembali</a>
            </form>
        </div>
    </div>
</div>