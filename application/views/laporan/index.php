<div class="container">
    <div class="card mt-2" style="width: 100%">
        <div class="card-header">
            <h5 class="card-title">Laporan</h5>
            <p class="card-text">PT. Kahatex</p>
        </div>

        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th width="10px">No.</th>
                        <th>NIK</th>
                        <th width="">Nama Karyawan</th>
                        <th>Nilai</th>
                        <th>Keterangan</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
        <div class="card-footer">
            Keterangan :
            <ul>
                <li></li>
            </ul>
        </div>
    </div>
</div>