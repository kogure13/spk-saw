<div class="container">
    <div class="card mt-2">
        <div class="card-header">
            <h5 class="card-title">Data Ranking</h5>
            <p class="card-text">PT. Kahatex</p>
        </div>

        <div class="card-body">
            <section id="tabel-nilai-awal" class="mb-2">
                <div class="card">
                    <div class="card-header">
                        Perhitungan awal
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="">
                                <thead>
                                    <tr>
                                        <th width="10px">No</th>
                                        <th width="110px">Nama</th>
                                        <th width="40px">K1</th>
                                        <th width="40px">K2</th>
                                        <th width="40px">K3</th>
                                        <th width="40px">K4</th>
                                        <th width="40px">K5</th>
                                        <th width="40px">K6</th>
                                        <th width="40px">K7</th>
                                        <th width="40px">K8</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1;foreach ($hitungAwal as $a): ?>
                                    <tr>
                                        <td><?=$i?></td>
                                        <td><?=$a['nama']?></td>
                                        <?php foreach ($this->Ranking_model->nilaiSifat($a['id']) as $item): ?>
                                        <td><?=$item['sub_kriteria']?></td>
                                        <?php endforeach;?>
                                    </tr>
                                    <?php $i++;endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>

            <section id="hitung-sifat" class="mb-2">
                <div class="card">
                    <div class="card-header">
                        Dihitung sesuai sifat
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="">
                                <thead>
                                    <tr>
                                        <th width="10px">No</th>
                                        <th>Nama</th>
                                        <th width="35px">K1</th>
                                        <th width="35px">K2</th>
                                        <th width="35px">K3</th>
                                        <th width="35px">K4</th>
                                        <th width="35px">K5</th>
                                        <th width="35px">K6</th>
                                        <th width="35px">K7</th>
                                        <th width="35px">K8</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1;foreach ($hitungAwal as $a): ?>
                                    <tr>
                                        <td><?=$i?></td>
                                        <td><?=$a['nama']?></td>
                                        <?php foreach ($this->Ranking_model->nilaiAwal($a['id']) as $item): ?>
                                        <td><?=$item['nilai']?></td>
                                        <?php endforeach;?>
                                    </tr>
                                    <?php $i++;endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>

            <section id="kali-bobot" class="mb-2">
                <div class="card">
                    <div class="card-header">
                        Dikali dengan bobot
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="">
                                <thead>
                                    <tr>
                                        <th width="10px">No</th>
                                        <th>Nama</th>
                                        <th width="35px">K1</th>
                                        <th width="35px">K2</th>
                                        <th width="35px">K3</th>
                                        <th width="35px">K4</th>
                                        <th width="35px">K5</th>
                                        <th width="35px">K6</th>
                                        <th width="35px">K7</th>
                                        <th width="35px">K8</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1;foreach ($hitungAwal as $a): ?>
                                    <tr>
                                        <td><?=$i?></td>
                                        <td><?=$a['nama']?></td>
                                        <?=$this->Ranking_model->nilaiBobot($a['id'], 'bagi')?>
                                    </tr>
                                    <?php $i++;endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>

            <section id="hasil-rekomendasi" class="mb-2">
                <div class="card">
                    <div class="card-header">
                        Dijumlah sesuai karyawan dan didapat hasil rekomendasi
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="">
                                <thead>
                                    <tr>
                                        <th width="10px">No</th>
                                        <th>Nama</th>
                                        <th width="35px">K1</th>
                                        <th width="35px">K2</th>
                                        <th width="35px">K3</th>
                                        <th width="35px">K4</th>
                                        <th width="35px">K5</th>
                                        <th width="35px">K6</th>
                                        <th width="35px">K7</th>
                                        <th width="35px">K8</th>
                                        <th width="35px">Total</th>
                                        <th>Keterangan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1;foreach ($hitungAwal as $a): ?>
                                    <tr>
                                        <td><?=$i?></td>
                                        <td><?=$a['nama']?></td>
                                        <?=$this->Ranking_model->nilaiBobot($a['id'], 'kali')?>
                                    </tr>
                                    <?php $i++;endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>