<?php

class Karyawan_model extends CI_model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Kriteria_model');
        $this->load->model('Ranking_model');
        $this->load->dbforge();
    }

    private function dataKaryawan()
    {
        $data = array(
            "nik" => $this->input->post('nik', true),
            "nama" => $this->input->post('nama', true),
            "gender" => $this->input->post('gender', true),
            "department" => $this->input->post('department', true),
            "email" => $this->input->post('email', true),
            "nohp" => $this->input->post('nohp', true),
            "username" => $this->input->post('nik', true),
            "password" => $this->input->post('nik', true),
            "role_user" => 2,
        );

        return $data;
    }

    public function getAllKaryawan()
    {
        return $this->db->get_where('karyawan', ['role_user' => 2])->result_array();
    }

    public function getKaryawanByID($id)
    {
        return $this->db->get_where('karyawan', ['id' => $id])->row_array();
    }

    public function tambahKaryawan()
    {
        $this->db->insert('karyawan', $this->dataKaryawan());
    }

    public function updateKaryawan()
    {
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('karyawan', $this->dataKaryawan());
    }

    public function addFormCuti()
    {
        $idKaryawan = $this->input->post('id', true);
        // ex. create a table
        // $drop = $this->dbforge->drop_table('nilai_saw', true);

        if (!$this->db->table_exists('nilai_saw')) {
            $fields = array(
                'karyawan' => array(
                    'type' => 'INT',
                ),
            );

            $data = $this->Kriteria_model->getAllKriteria();

            $x=1;
            foreach ($data as $kriteria) {
                $fields[] = $kriteria['kode'] . " INT ";
            }

            // array_merge($row, $fields);

            // var_dump($fields);die;
            
            $this->dbforge->add_field($fields);
            $this->dbforge->create_table('nilai_saw');
            // $this->dbforge->modify_column('nilai_saw', $new_fields);
            // die;
        }

        // insert data cuti
        $i = 1;
        while (isset($_POST['q' . $i])) {
            $jawaban = $_POST['q' . $i];
            $nilai = $this->Kriteria_model->getSubKriteriaByID($jawaban);
            $data = array(
                "id_karyawan" => $idKaryawan,
                "kriteria" => $jawaban,
                "nilai" => $nilai['nilai'],
            );

            $this->db->insert('karyawan_nilai', $data);
            $i++;
            // var_dump($data);
        }

        // die;        
        $data['field'] = "*";
        $data = $this->Ranking_model->getAllNilai($data);
        foreach ($data as $key=>$value) {
            if($value['id_karyawan'] == $idKaryawan) {
                $field[] = $value['nilai'];
            }
        }
        // var_dump($field); die;
        $dtNilai = array(
            "karyawan" => $idKaryawan,
            "K001" => $field[0],
            "K002" => $field[1],
            "K003" => $field[2],
            "K004" => $field[3],
            "K005" => $field[4],
            "K006" => $field[5],
            "K007" => $field[6],
            "K008" => $field[7],
        );
        $this->db->insert('nilai_saw', $dtNilai);
    }
}