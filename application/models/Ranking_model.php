<?php

class Ranking_model extends CI_model
{
    public function hitungAwal()
    {
        $this->db->select('karyawan.id, karyawan.nama, kriteria.id as idk, karyawan_nilai.nilai');
        $this->db->from('karyawan_nilai');
        $this->db->join('karyawan', 'karyawan_nilai.id_karyawan = karyawan.id');
        $this->db->join('sub_kriteria', 'karyawan_nilai.kriteria = sub_kriteria.id');
        $this->db->join('kriteria', 'sub_kriteria.id_kriteria = kriteria.id');
        $this->db->group_by('id_karyawan');
        $query = $this->db->get();

        return $query->result_array();
    }

    public function nilaiAwal($id)
    {
        $this->db->select('sub_kriteria.nilai');
        $this->db->from('karyawan_nilai');
        $this->db->join('karyawan', 'karyawan_nilai.id_karyawan = karyawan.id');
        $this->db->join('sub_kriteria', 'karyawan_nilai.kriteria = sub_kriteria.id');
        $this->db->join('kriteria', 'sub_kriteria.id_kriteria = kriteria.id');
        $this->db->where('id_karyawan', $id);
        $query = $this->db->get();

        return $query->result_array();
    }

    public function nilaiSifat($id)
    {
        $this->db->select('sub_kriteria');
        $this->db->from('karyawan_nilai');
        $this->db->join('karyawan', 'karyawan_nilai.id_karyawan = karyawan.id');
        $this->db->join('sub_kriteria', 'karyawan_nilai.kriteria = sub_kriteria.id');
        $this->db->join('kriteria', 'sub_kriteria.id_kriteria = kriteria.id');
        $this->db->where('id_karyawan', $id);
        $query = $this->db->get();

        return $query->result_array();
    }

    public function nilaiBobotKriteria($id) {
        $this->db->select('kriteria.bobot');
        $this->db->from('karyawan_nilai');
        $this->db->join('karyawan', 'karyawan_nilai.id_karyawan = karyawan.id');
        $this->db->join('sub_kriteria', 'karyawan_nilai.kriteria = sub_kriteria.id');
        $this->db->join('kriteria', 'sub_kriteria.id_kriteria = kriteria.id');
        $this->db->where('id_karyawan', $id);
        $query = $this->db->get();

        return $query->result_array();
    }

    public function getAllNilai($data)
    {
        $this->db->select($data['field']);
        $this->db->from('karyawan_nilai');
        $this->db->join('karyawan', 'karyawan_nilai.id_karyawan = karyawan.id');
        $this->db->join('sub_kriteria', 'karyawan_nilai.kriteria = sub_kriteria.id');
        $this->db->join('kriteria', 'sub_kriteria.id_kriteria = kriteria.id');
        if (isset($data['where'])) {
            $this->db->where($data['where']);
        }

        $query = $this->db->get();

        return $query->result_array();
    }

    public function nilaiBobot($id, $view)
    {
        $this->db->select('MIN(K001) AS K1, MIN(K002) AS K2, MIN(K003) AS K3, MIN(K004) AS K4, MIN(K005) AS K5, MIN(K006) AS K6, MIN(K007) AS K7, MIN(K008) AS K8');
        $this->db->from('nilai_saw');
        $min = $this->db->get()->result_array();

        foreach ($min as $nilaiMin) {
            $minK1 = $nilaiMin['K1'];
            $minK2 = $nilaiMin['K2'];
            $minK3 = $nilaiMin['K3'];
            $minK4 = $nilaiMin['K4'];
            $minK5 = $nilaiMin['K5'];
            $minK6 = $nilaiMin['K6'];
            $minK7 = $nilaiMin['K7'];
            $minK8 = $nilaiMin['K8'];
        }

        if($view == 'bagi') {
            $data = $this->db->get_where('nilai_saw', ['karyawan' => $id])->result_array();
            foreach ($data as $bagiMin) {
                echo "<td>".$HK1=$minK1/$bagiMin['K001']."</td>";
                echo "<td>".$HK2=$minK2/$bagiMin['K002']."</td>";
                echo "<td>".$HK3=$minK3/$bagiMin['K003']."</td>";
                echo "<td>".$HK4=$minK4/$bagiMin['K004']."</td>";
                echo "<td>".$HK5=$minK5/$bagiMin['K005']."</td>";
                echo "<td>".$HK6=$minK6/$bagiMin['K006']."</td>";
                echo "<td>".$HK7=$minK7/$bagiMin['K007']."</td>";
                echo "<td>".$HK8=$minK8/$bagiMin['K008']."</td>";
            }
        } else {
            $bobot = $this->nilaiBobotKriteria($id);      
            $data = $this->db->get_where('nilai_saw', ['karyawan' => $id])->result_array();
            $i=0;
            foreach ($bobot as $nBobot) {
                $HK[$i] = $nBobot['bobot']/100;
                $i++;
            }                  

            $j=0;
            $total = 0;
            foreach ($data as $bagiMin) {
                echo "<td>".$a1=$HK[0]*($minK1/$bagiMin['K001'])."</td>";
                echo "<td>".$a2=$HK[1]*($minK2/$bagiMin['K002'])."</td>";
                echo "<td>".$a3=$HK[2]*($minK3/$bagiMin['K003'])."</td>";
                echo "<td>".$a4=$HK[3]*($minK4/$bagiMin['K004'])."</td>";
                echo "<td>".$a5=$HK[4]*($minK5/$bagiMin['K005'])."</td>";
                echo "<td>".$a6=$HK[5]*($minK6/$bagiMin['K006'])."</td>";
                echo "<td>".$a7=$HK[6]*($minK7/$bagiMin['K007'])."</td>";
                echo "<td>".$a8=$HK[7]*($minK8/$bagiMin['K008'])."</td>";
                $total = $this->getTotal($a1)+$this->getTotal($a2)+$this->getTotal($a3)+$this->getTotal($a4)+$this->getTotal($a5)+$this->getTotal($a6)+$this->getTotal($a7)+$this->getTotal($a8);
                echo "<td>".$hasil = round(($total*100), 4)."</td>";
                echo "<td>".$this->hasil($hasil)."</td>";
                $j++;
            }
        }
    }

    public function getTotal($a){
        return (float)$a;
    }

    public function hasil($hasil) {
        if($hasil >= 80) {
            return "SURAT SEHAT";
        } else if($hasil >= 50 && $hasil < 80) {
            return "SWAB PCR";
        }else {
            return "SWAB PCR DAN KARANTINA";
        } 
    }

}