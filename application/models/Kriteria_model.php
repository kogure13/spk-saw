<?php

class Kriteria_model extends CI_model
{

    private function dataKriteria()
    {
        $data = array(
            "kode" => $this->input->post('kode', true),
            "kriteria" => $this->input->post('kriteria', true),
            "sifat" => $this->input->post('sifat', true),
            "bobot" => $this->input->post('bobot', true),
            "pertanyaan" => $this->input->post('pertanyaan', true),
        );

        return $data;
    }

    private function dataSubKriteria()
    {
        $data = array(
            "id_kriteria" => $this->input->post('id_kriteria', true),
            "sub_kriteria" => $this->input->post('sub_kriteria', true),
            "nilai" => $this->input->post('nilai', true),
        );

        return $data;
    }

    public function getAllKriteria()
    {
        return $this->db->get('kriteria')->result_array();
    }

    public function getAllSubKriteria()
    {
        return $this->db->get('sub_kriteria')->result_array();
    }

    public function tambahKriteria()
    {
        $this->db->insert('kriteria', $this->dataKriteria());
    }

    public function tambahSubKriteria()
    {
        $this->db->insert('sub_kriteria', $this->dataSubKriteria());
    }

    public function updateKriteria()
    {
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('kriteria', $this->dataKriteria());
    }

    public function updateSubKriteria()
    {
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('sub_kriteria', $this->dataSubKriteria());
    }

    public function delete($id)
    {
        return $this->db->delete('kriteria', ['id' => $id]);
    }

    public function getKriteriaByID($id)
    {
        return $this->db->get_where('kriteria', ['id' => $id])->row_array();
    }

    public function getSubKriteriaByID($id)
    {
        return $this->db->get_where('sub_kriteria', ['id' => $id])->row_array();
    }

    public function getSubKriteriaByIDKriteria($id)
    {
        return $this->db->get_where('sub_kriteria', ['id_kriteria' => $id])->result_array();
    }
}