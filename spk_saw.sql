-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 21, 2021 at 02:20 AM
-- Server version: 8.0.26-0ubuntu0.20.04.3
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spk_saw`
--

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `id` int NOT NULL,
  `nik` varchar(20) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `department` varchar(25) NOT NULL,
  `email` varchar(20) NOT NULL,
  `nohp` varchar(15) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(12) NOT NULL,
  `role_user` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=armscii8;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`id`, `nik`, `nama`, `gender`, `department`, `email`, `nohp`, `username`, `password`, `role_user`) VALUES
(1, 'KHT-001', 'HENDRA', 'Laki-laki', 'Admin Office', 'hendra@kahatex.com', '0897654321', '', '', 2),
(2, 'KHT-002', 'RIZKY', 'LAKI-LAKI', '', '', '', '', '', 2),
(3, 'KHT-003', 'MASAYU', 'PEREMPUAN', '', '', '', '', '', 2),
(4, 'KHT-004', 'BUDI', 'LAKI-LAKI', '', '', '', '', '', 2),
(5, 'KHT-005', 'OVIN', 'PEREMPUAN', '', '', '', '', '', 2),
(6, 'KHT-006', 'BENI', 'LAKI-LAKI', '', '', '', '', '', 2),
(7, 'KHT-007', 'ADE', 'LAKI-LAKI', '', '', '', '', '', 2),
(8, 'KHT-008', 'PUTRI', 'PEREMPUAN', '', '', '', '', '', 2),
(9, 'KHT-009', 'DEDE', 'LAKI-LAKI', '', '', '', '', '', 2),
(10, 'KHT-010', 'ROLAN', 'Perempuan', 'Admin Office', 'ro_lan@kahatex.com', '0887654321', '', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `karyawan_nilai`
--

CREATE TABLE `karyawan_nilai` (
  `id` int NOT NULL,
  `id_karyawan` int NOT NULL,
  `tanggal` date NOT NULL,
  `kriteria` int NOT NULL,
  `clock_out` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=armscii8;

-- --------------------------------------------------------

--
-- Table structure for table `kriteria`
--

CREATE TABLE `kriteria` (
  `id` int NOT NULL,
  `kode` varchar(25) CHARACTER SET armscii8 COLLATE armscii8_general_ci NOT NULL,
  `kriteria` varchar(30) NOT NULL,
  `sifat` varchar(25) NOT NULL,
  `bobot` varchar(2) CHARACTER SET armscii8 COLLATE armscii8_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=armscii8;

--
-- Dumping data for table `kriteria`
--

INSERT INTO `kriteria` (`id`, `kode`, `kriteria`, `sifat`, `bobot`) VALUES
(1, 'K001', 'Jenis Acara', 'Cost', '5'),
(2, 'K002', 'Daerah', 'Cost', '4'),
(3, 'K003', 'Jumlah Orang', 'Cost', '5'),
(4, 'K004', 'Tempat Acara', 'Cost', '5'),
(5, 'K005', 'Makan Bersama', 'Cost', '3'),
(6, 'K006', 'Zona Merah', 'Cost', '5'),
(7, 'K007', 'ODP/PDP', 'Cost', '5'),
(8, 'K008', 'Protokol Kesehatan', 'Cost', '3');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int NOT NULL,
  `role` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=armscii8;

-- --------------------------------------------------------

--
-- Table structure for table `sub_kriteria`
--

CREATE TABLE `sub_kriteria` (
  `id` int NOT NULL,
  `id_kriteria` int NOT NULL,
  `sub_kriteria` varchar(50) CHARACTER SET armscii8 COLLATE armscii8_general_ci NOT NULL,
  `nilai` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=armscii8;

--
-- Dumping data for table `sub_kriteria`
--

INSERT INTO `sub_kriteria` (`id`, `id_kriteria`, `sub_kriteria`, `nilai`) VALUES
(1, 1, 'Pernikahaan', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `karyawan_nilai`
--
ALTER TABLE `karyawan_nilai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kriteria`
--
ALTER TABLE `kriteria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_kriteria`
--
ALTER TABLE `sub_kriteria`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kriteria` (`id_kriteria`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `karyawan_nilai`
--
ALTER TABLE `karyawan_nilai`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kriteria`
--
ALTER TABLE `kriteria`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sub_kriteria`
--
ALTER TABLE `sub_kriteria`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `sub_kriteria`
--
ALTER TABLE `sub_kriteria`
  ADD CONSTRAINT `sub_kriteria_ibfk_1` FOREIGN KEY (`id_kriteria`) REFERENCES `kriteria` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
